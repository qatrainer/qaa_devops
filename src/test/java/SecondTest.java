import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class SecondTest extends BaseTest{

    @Test
    public void AutomationpracticeTest() throws Exception {
        Reporter.log(String.format("AutomationpracticeT Test Started %s",Thread.currentThread().getId()),true);
        getDriver().navigate().to("http://automationpractice.com/);
        Assert.assertEquals(getDriver().getTitle(), "My Store");
        Reporter.log(String.format("Myntra Test Ended %s",Thread.currentThread().getId()),true);
    }

    @Test
    public void RyanairTest() throws Exception {
        Reporter.log(String.format("RyanAir Test Started %s",Thread.currentThread().getId()),true);
        getDriver().navigate().to("https://www.ryanair.com/gb/en");
        Assert.assertEquals(getDriver().getTitle(), "Official Ryanair website | Book direct for the lowest fares | Ryanair.com");
        Reporter.log(String.format("ThoughtWorks Test Ended %s",Thread.currentThread().getId()),true);
    }
}
